#!/usr/bin/python
from xml.dom import minidom
import re

def pconvert(pl):
	first = True
	result = []
	for p in pl:
		if first:
			first = False
			result.append(['M', p%5*100*2, p/5*100*2])
		else:
			result.append(['L', p%5*100*2, p/5*100*2])
	return result

states = {}

dom = minidom.parse('map.svg')
for e in dom.getElementsByTagName('path'): 
	n = e.getAttributeNode('id').nodeValue.strip('-')
	d =  e.getAttributeNode('d').nodeValue
	if len(n) == 2 and n not in ['AK', 'IH']:
		tl = re.findall('([A-Z])\s+(\d+\.\d+),(\d+\.\d+)',d)
		states[n]=[ [str(c), float(x), float(y)] for (c,x,y) in tl ]

f = open('states.csv')
lines =f.readlines()
f.close()
codes = {}
for l in lines:
	cid = re.findall('s[A-Z]{2}n', l)[0].strip('sn')
	cv =  [ c.strip('"') for c in re.findall('"[A-Z]{2}"', l) ]
	if codes.get(cid) :
		codes[cid].append(cv)
	else:
		codes[cid] = cv

indices = {}
count = 0
for c in codes:
	indices[c] = count
	count +=1
nb = {}
for c in codes:
	nb[c] = [indices[n] for n in codes[c]]

f = open ('states.js', 'w')
for c in indices:
	temp = '\t\tthis.areas.push(new Area({game:this,id:%s,points:%s,neighbours:%s}));' % (
		indices[c],
		states[c],
		nb[c]
	)
	f.write(temp)
	f.write('\r\n')
f.flush()
f.close()


